var Jimp = require('jimp');
var loaderUtils = require('loader-utils');

module.exports = function(image) {
  var options = loaderUtils.getOptions(this);
  var maxWidth = 1600;
  var maxHeight = 1200;
  if (options) {
    maxWidth = isNaN(parseInt(options.w)) ? 1600 : parseInt(options.w);
    maxHeight = isNaN(parseInt(options.h)) ? 1200 : parseInt(options.h);
  }
  this.cacheable && this.cacheable();
  var callback = this.async();
  Jimp.read(image).then(function(img) {
    if (img.bitmap.width > maxWidth || img.bitmap.height > maxHeight) {
      img
        .scaleToFit(maxWidth, maxHeight)
        .quality(60)
        .getBuffer(Jimp.AUTO, function(err, buff) {
          callback(null, buff);
        });
    } else {
      callback(null, image);
    }
  });
};
module.exports.raw = true;
